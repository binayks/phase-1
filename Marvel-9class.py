import numpy as np
from numpy.linalg import eig as npeig
from numpy.linalg import inv as npinv
import torch
cluster_train = np.load("Trainingdata_10class_all_images.npy", allow_pickle=True)
cluster_train = torch.Tensor([i[0]  for i in cluster_train]).view(-1, 64, 64)
cluster_train = cluster_train/255.0

def sortbya(a,b):
    c = zip(a,b)
    x = sorted(c, key=lambda x: x[0])
    newa, newb = zip(*x)
    return newa,newb

def convmtx2(H, M, N):

    P, Q = H.shape
    blockHeight = int(M + P - 1)
    blockWidth = int(M)
    blockNonZeros = int(P * M)
    N_blockNonZeros = int(N * blockNonZeros)
    totalNonZeros = Q * N_blockNonZeros

    THeight = int((N + Q - 1) * blockHeight)
    TWidth = int(N * blockWidth)

    Tvals = np.zeros(totalNonZeros, dtype=H.dtype)
    Trows = np.zeros(totalNonZeros, dtype=int)
    Tcols = np.zeros(totalNonZeros, dtype=int)

    c = np.repeat(np.arange(1, M + 1)[:, np.newaxis], P, axis=1)
    r = np.repeat(np.reshape(c + np.arange(0, P)[np.newaxis], (-1, 1), order='F'), N, axis=1)
    c = np.repeat(c.flatten('F')[:, np.newaxis], N, axis=1)

    colOffsets = np.arange(N, dtype=int) * M
    colOffsets = (np.repeat(colOffsets[np.newaxis], M * P, axis=0) + c).flatten('F') - 1

    rowOffsets = np.arange(N, dtype=int) * blockHeight
    rowOffsets = (np.repeat(rowOffsets[np.newaxis], M * P, axis=0) + r).flatten('F') - 1

    for k in range(Q):
        val = (np.tile((H[:, k]).flatten(), (M, 1))).flatten('F')
        first = int(k * N_blockNonZeros)
        last = int(first + N_blockNonZeros)
        Trows[first:last] = rowOffsets
        Tcols[first:last] = colOffsets
        Tvals[first:last] = np.tile(val, N)
        rowOffsets += blockHeight

    T = np.zeros((THeight, TWidth), dtype=H.dtype)
    T[Trows, Tcols] = Tvals
    return T
def make_weights(images,clusters,channel):
    classes = np.unique(clusters)
    nclasses = len(classes)
    print(nclasses,'cluster based classes')

    #sizes hard coded now, need to change for other sets
    a = np.zeros((5*5,5*5,nclasses))
    mask = np.zeros((64 + 5 - 1, 64 + 5 - 1))
    i1 = int((5 - 1) / 2)
    i2 = int(64 + 5 - 1 - (5 - 1) / 2)
    i3 = int((5 - 1) / 2)
    i4 = int(64 + 5 - 1 - (5 - 1) / 2)
    mask[i1:i2, i3:i4] = np.ones((64, 64))
    valid_rows = np.where(mask.ravel() == 1)
    valid_rows = valid_rows[0]
    print(valid_rows.shape)


    for label in range(nclasses):
        indices = np.where(clusters == label)
        indices = indices[0]
        clustersize = len(indices)
        #print('cluster ', label, 'with ', clustersize , 'members')
        #print('indices', indices, 'clustersize', clustersize)
        for imageidx in range(clustersize):
            x = np.double(images[indices[imageidx], :, :])
            t = convmtx2(x,5,5)
            t = np.flip(t,1)
            t = t[valid_rows]
            tsquare = np.matmul(t.transpose(), t)
            a[:,:,label] = a[:,:,label] + tsquare
        a[:,:,label] = a[:,:,label]/clustersize

    weight = []
    for label in range(nclasses):
        p = a[:,:,label]

        q = np.matmul(npinv(np.sum(a, 2) - p), p)
        delta, phi = npeig(q)
        phi = phi.transpose()
        sdelta, sphi = sortbya(delta, phi)
        topvectors = sphi[22:25]
        weight.append(np.stack(topvectors))

    weight = np.concatenate((weight), axis=0)
    weight = weight.reshape(150, 1, 5, 5)
    return weight

def make_weights_rgb(images,clusters):
    weights = []
    for channel in range(1):
        weights.append(make_weights(images,clusters,channel))
    weights = np.concatenate((weights),axis=1)
    return weights

clusters = np.load('cluster_tsne.npy')
#images= np.load("TSNE-Visualization.npy")
images = cluster_train
print('clustershape', len(clusters))
print(images.shape)
Y = np.reshape(images, (72772, 64, 64, 1))
print(Y.shape)
Y = Y[0:100,:,:,:]
channel = 0
#print(images[0])

a = make_weights(images,clusters,channel)
print(a.shape)
