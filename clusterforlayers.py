import pandas as pd
import torch
import numpy as np
from sklearn import manifold
from sklearn.manifold import TSNE
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

total_training_data = np.load("Trainingdata_10class_all_images.npy", allow_pickle=True)
print('length of total_training_data', len(total_training_data))

cluster_data = total_training_data

index = int(len(cluster_data) /6)
cluster_data = cluster_data[:index]


def tsne_cluster():
    images = []
    for im in cluster_data:
        images.append(np.array(im[0]))
    images = np.stack(images)
    images = images.reshape(len(cluster_data), 64 * 64)
    print('image shape', len(images))


    kmeans = KMeans(n_clusters=50, init='k-means++', max_iter=300, n_init=10)
    ans = kmeans.fit_predict(images)
    centers = kmeans.cluster_centers_

    print('Visualization of TSNE for clustering')

    images = np.append(images, centers, axis=0)

    test_images = TSNE(random_state=45).fit_transform(images)

    #np.save('smallcluster.npy', test_images)

    index = int(len(images)/2)
    test_images1 = test_images[:-index]

    print('test1 images shape', test_images1.shape)
    test_images2 = test_images[-index:]
    print('test 2 images shape', test_images2.shape)
    fig, ax = plt.subplots()

    c = np.random.randint(1,10+1, size=index)



    scatter1 = ax.scatter(test_images1[:, 0], test_images1[:, 1], c= c, s= 5, cmap="tab10")

    scatter2 = ax.scatter(test_images2[:50, 0], test_images2[:50, 1], c= 'black', s=15)

    #legend1 = ax.legend(*scatter1.legend_elements(num=2),loc="upper right", title="Cluster")

    #ax.add_artist(legend1)


    plt.show()


tsne_cluster()

